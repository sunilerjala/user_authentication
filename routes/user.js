var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var user = require('../models/User.js');
var passport = require('passport');
require('../config/passport')(passport);

/* GET User */
router.get('/', passport.authenticate('jwt', { session: false}), function(req, res) {
  
  var token = getToken(req.headers);
  if (token) {
    
    
    let usert = req.user.usertype;
    let username = req.user.username;
    if(usert === 'superadmin') {
      console.log('coming into the superadmin')
        user.find({ usertype: { $in : ['admin', 'user']} }, (err, users) => {
            if(err) throw err;
            users.push(req.user)
            res.json(users);
        });
    } else if(usert === 'admin') {
      console.log('coming into the admin')
        user.find({ usertype: 'user'}, (err, users) => {
            if(err) throw err;
            users.push(req.user);
            res.json(users);
        });
    } else if(usert === 'user') {

        //  res.json(req.user);
        user.find({ username: username}, (err, users) => {
          if(err) throw err;
        
          res.json(users);
      });
    }
  }
   else {
     
    return res.status(401).send({success: false, msg: 'Unauthorized.'});
  }
});








// /* save user */
// router.post('/', passport.authenticate('jwt', { session: false}), function(req, res) {
//   var token = getToken(req.headers);
//   if (token) {
//     user.create(req.body, function (err, post) {
//       if (err) return next(err);
//       res.json(post);
//     });
//   } else {
//     return res.status(403).send({success: false, msg: 'Unauthorized.'});
//   }
// });




getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      console.log(parted[1])
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};




router.get('/:id', function(req, res, next) {
  user.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});


router.put('/:id', function(req, res, next) {
  user.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    console.log(req.body);
    console.log(req.params);
    if (err) return next(err);
    res.json(post);
  });
});



module.exports = router;
