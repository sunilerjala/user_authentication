import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class Edit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {}
    };
  }

  componentDidMount() {
    axios.get('/api/user/'+this.props.match.params.id)
      .then(res => {
        this.setState({ user: res.data });
        console.log(this.state.user);
      });
  }

  onChange = (e) => {
    const state = this.state.user
    state[e.target.name] = e.target.value;
    this.setState({user:state});
  }

  onSubmit = (e) => {

    e.preventDefault();

    const { username,usertype } = this.state.user;
    console.log(username);
    axios.put('/api/user/'+this.props.match.params.id, { usertype,username })
      .then((result) => {
        
        this.props.history.push("/")
      });
  }

  render() {
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              EDIT User
            </h3>
          </div>
          <div class="panel-body">
            <h4><Link to={`/`}><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> User List</Link></h4>
            <form class="form-signin" onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="username">Username</label>
                <input type="email" class="form-control" name="username" value={this.state.user.username} onChange={this.onChange} placeholder="username" />
              </div>
              <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Edit;